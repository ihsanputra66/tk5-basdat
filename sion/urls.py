"""sion URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url, include
from django.views.generic import RedirectView
from django.contrib import admin
from django.views.generic.base import RedirectView
import fitur5.urls as fitur5
import fitur6.urls as fitur6

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^organisasi/', include(fitur5, namespace='list-organisasi')),
    url(r'^donasi/', include(fitur6, namespace='donation')),
]
